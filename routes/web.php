<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
Route::get('/postgallery','Frontend\postgalleryControllers@index')->name('postgallery');
*/
Route::get('/','Frontend\AccueilControllers@index')->name('home');
Route::get('/portfolio','Frontend\PortfolioControllers@index')->name('portfolio');
Route::get('/fashion','Frontend\FashionControllers@index')->name('fashion');
Route::get('/business','Frontend\BusinessControllers@index')->name('business');
Route::get('/single','Frontend\SingleControllers@index')->name('single');
Route::get('/reel','Frontend\ReelControllers@index')->name('reel');
Route::get('/grid','Frontend\GridControllers@index')->name('grid');
Route::get('/list','Frontend\ListControllers@index')->name('list');
Route::get('/caroussel','Frontend\CarouselControllers@index')->name('caroussel');
Route::get('/postgallery','Frontend\PostgalleryControllers@index')->name('postgallery');
Route::get('/post','Frontend\PostControllers@index')->name('post');
Route::get('/postVideo','Frontend\PostVideoControllers@index')->name('postVideo');
Route::get('/postNo','Frontend\PostNoControllers@index')->name('postNo');
Route::get('/masonry','Frontend\MasonryControllers@index')->name('masonry');
Route::get('/alternative','Frontend\AlternativeControllers@index')->name('alternative');
Route::get('/fullwidth','Frontend\FullwidthControllers@index')->name('fullwidth');
Route::get('/porfolioCa','Frontend\PorfolioCaControllers@index')->name('porfolioCa');
Route::get('/singlePortfolio','Frontend\SinglePortfolioControllers@index')->name('singlePortfolio');
Route::get('/video','Frontend\VideoControllers@index')->name('video');
Route::get('/about','Frontend\AboutControllers@index')->name('about');
Route::get('/contact','Frontend\ContactControllers@index')->name('contact');
Route::get('/404','Frontend\ErrorPageControllers@index')->name('404');
Route::get('/coming','Frontend\ComingControllers@index')->name('coming');
