<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">

    <link rel="shortcut icon" href="{{asset('assets/images/favicon/favicon.ico')}}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{asset('assets/images/favicon/apple-touch-icon.png')}}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('assets/images/favicon/apple-touch-icon-57x57.png')}}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/images/favicon/apple-touch-icon-72x72.png')}}" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/images/favicon/apple-touch-icon-76x76.png')}}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/images/favicon/apple-touch-icon-114x114.png')}}" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('assets/images/favicon/apple-touch-icon-120x120.png')}}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('assets/images/favicon/apple-touch-icon-144x144.png')}}" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/images/favicon/apple-touch-icon-152x152.png')}}" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/images/favicon/apple-touch-icon-180x180.png')}}" />

    <title>Benedict - by Distinctive Themes</title>
    <link href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,100,700,300|Nothing+You+Could+Do' rel='stylesheet' type='text/css'>

    <link href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/pe-icons/css/pe-icon-7-stroke.css')}}" rel="stylesheet">

    <link href="{{asset('assets/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/plugins.css')}}" rel="stylesheet">
    <link href="{{asset('assets/style.css')}}" rel="stylesheet">

</head>

<body class="top-navigation pushy-left-side pushy-left-open">

<nav class="pushy pushy-left">
    <ul class="list-unstyled">
        <li><a class="logo" href="{{route('home')}}"><img alt="" class="logo img-responsive" src="{{asset('assets/images/logo-light.png')}}"></a></li>
        <li class="pushy-submenu">
            <a class="submenu-link" href="#">Home <i class="fa fa-angle-down"></i></a>
            <ul>
                <li><a href="{{route('home')}}">Home</a></li>
                <li><a href="{{route('portfolio')}}">Home Portfolio</a></li>
                <li><a href="{{route('fashion')}}">Home Fashion</a></li>
                <li><a href="{{route('business')}}">Home Business</a></li>
                <li><a href="{{route('single')}}">Home Single Page</a></li>
            </ul>
        </li>
        <li class="pushy-submenu">
            <a class="submenu-link" href="#">Blog <i class="fa fa-angle-down"></i></a>
            <ul>
                <li><a href="{{route('reel')}}">Blog Reel</a></li>
                <li><a href="{{route('grid')}}">Blog Grid</a></li>
                <li><a href="{{route('list')}}">Blog List</a></li>
                <li><a href="{{route('caroussel')}}">Blog Carousel</a></li>
                <li><a href="{{route('post')}}">Single Post</a></li>
                <li><a href="{{route('postgallery')}}">Single Post Gallery</a></li>
                <li><a href="{{route('postVideo')}}">Single Post Video</a></li>
                <li><a href="{{route('postNo')}}">Single Post No Sidebar</a></li>
            </ul>
        </li>
        <li class="pushy-submenu">
            <a class="submenu-link" href="#">Portfolio <i class="fa fa-angle-down"></i></a>
            <ul>
                <li><a href="{{route('masonry')}}">Portfolio Masonry</a></li>
                <li><a href="{{route('alternative')}}">Portfolio Masonry Alternative</a></li>
                <li><a href="{{route('fullwidth')}}">Portfolio Masonry Fullwidth</a></li>
                <li><a href="{{route('porfolioCa')}}">Portfolio Carousel</a></li>
                <li><a href="{{route('singlePortfolio')}}">Single Portfolio</a></li>
                <li><a href="{{route('video')}}">Single Portfolio Video</a></li>
            </ul>
        </li>
        <li class="pushy-submenu">
            <a class="submenu-link" href="#">Pages <i class="fa fa-angle-down"></i></a>
            <ul>
                <li><a href="{{route('about')}}">About Us</a></li>
                <li><a href="{{route('contact')}}">Contact Us</a></li>
                <li><a href="{{route('404')}}">404</a></li>
                <li><a href="{{route('coming')}}">Coming Soon</a></li>
            </ul>
        </li>
        <li class="pushy-submenu">
            <a class="submenu-link" href="#">Features <i class="fa fa-angle-down"></i></a>
            <ul>
                <li class="pushy-submenu">
                    <a class="submenu-link" href="#">Blog Feeds <i class="fa fa-angle-down"></i></a>
                    <ul>
                        <li><a href="{{route('grid')}}">Blog Style 1</a></li>
                        <li><a href="{{route('caroussel')}}">Blog Style 2</a></li>
                        <li><a href="{{route('list')}}">Blog Style 3</a></li>
                        <li><a href="{{route('reel')}}">Blog Style 4</a></li>
                    </ul>
                </li>
                <li class="pushy-submenu">
                    <a class="submenu-link" href="#">Team Feeds <i class="fa fa-angle-down"></i></a>
                    <ul>
                        <li><a href="{{route('home')}}#our-staff">Team Style 1</a></li>
                        <li><a href="{{route('fashion')}}#our-team">Team Style 2</a></li>
                        <li><a href="{{route('business')}}#our-team">Team Style 3</a></li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>
</nav>

<!-- Site Overlay -->
<div class="site-overlay"></div>

<div id="master-wrapper">

    <div class="preloader">
        <div class="preloader-img">
            <span class="loading-animation animate-flicker"><img src="{{asset('assets/images/loading.gif')}}" alt="loading" /></span>
        </div>
    </div>

    <div class="nav-wrapper smoothie">
        <div class="nav-inner smoothie">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3 col-xs-6">
                        <a class="logo" href="{{route('home')}}"><img alt="" class="logo img-responsive" src="{{asset('assets/images/logo-light.png')}}"></a>
                    </div>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false" aria-controls="navbar">
                        <a class="tcon tcon-menu--minus menu-btn" aria-label="toggle menu">
                            <span class="tcon-menu__lines" aria-hidden="true"></span>
                            <span class="tcon-visuallyhidden">toggle menu</span>
                        </a>
                    </button>
                    <div class="col-sm-9 col-xs-6 hidden-xs nopaddingleftright">
                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-search"></i></a>
                                    <ul class="dropdown-menu">
                                        <div class="nav-search-form-wrapper">
                                            <form class="form-inline">
                                                <button type="submit" class="btn btn-default pull-right"><i class="glyphicon glyphicon-search"></i></button><input type="text" class="form-control pull-left" placeholder="Search">
                                            </form>
                                        </div>
                                    </ul>
                                </li>
                                <li class="share-li twitter-bg">
                                    <a href="#"><span>Tweet</span> <i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="share-li facebook-bg">
                                    <a href="#"><span>Share</span> <i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="share-li gplus-bg">
                                    <a href="#"><span>+1</span> <i class="fa fa-google-plus"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @yield('pages')


    <footer class="pure-white-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <div class="text-widget widget">
                        <h4 class="widget-title mb40">Location</h4>
                        <div class="widget-content">
                            <p>Conveniently enhance high-quality imperatives vis-a-vis team driven technologies. Intrinsicly fashion economically sound communities rather than principle-centered deliverables. Synergistically impact.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="useful-link-widget widget">
                        <h4 class="widget-title mb40">Pages</h4>
                        <div class="widget-content">
                            <div class="useful-link-list">
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <ul class="list-unstyled">
                                            <li>
                                                <i class="fa fa-angle-right"></i><a href="{{route('home')}}"> Home</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i><a href="#"> Portfolio</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i><a href="#"> Support</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i><a href="#"> Why Us?</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i><a href="#"> Social Media</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i><a href="#"> Site Map</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <ul class="list-unstyled">
                                            <li>
                                                <i class="fa fa-angle-right"></i><a href="#"> Company</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i><a href="#"> Latest News</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i><a href="#"> Partners</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i><a href="#"> Blog Post</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i><a href="#"> Help Topic</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i><a href="#"> Policies</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="mailing-widget widget">
                        <h4 class="widget-title mb40">Mailing List</h4>
                        <div class="content-wiget">
                            <p class="mb40">Subscribe to our newsletter for the latest updates and offers.</p>
                            <form action="{{route('home')}}">
                                <div class="input-group">
                                    <input class="form-control form-email-widget" placeholder="Email address" type="text"><span class="input-group-btn"><input class="btn btn-email" type="submit" value="✓"></span>
                                </div>
                            </form>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <a href="#" id="back-to-top"><i class="fa fa-angle-up"></i></a>

</div>

<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/plugins.js')}}"></script>
<script src="{{asset('assets/js/owl-carousel.js')}}"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="{{asset('assets/js/init.js')}}"></script>

</body>
</html>

