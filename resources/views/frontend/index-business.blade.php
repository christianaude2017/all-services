@extends('app')

@section('pages')
    <header id="home" class="business-slider fullheight">
        <div class="fullheight">
            <div class="container fullheight">
                <div class="jumbotron">
                    <div class="row">
                        <div class="col-sm-11 col-xs-12">
                            <h1><small>Provider of</small><br>
                                Bespoke Solutions</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="hover-shift smoothie match-height parallax" data-parallax="scroll" data-image-src="{{asset('assets/images/business/business-slide-3.jpg')}}" data-speed="0.8" data-mh="page-splitter">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-xs-12 dark-overlay fill-left smoothie match-height" data-mh="page-splitter">
                    <div class="section-inner">
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10">
                                <h3 class="section-title">Why Choose Uneek?</h3>
                                <h5 class="section-sub-title mb40">Let us explain.</h5>
                                <p class="mb40">Holisticly orchestrate superior total linkage rather than end-to-end internal or "organic" sources. Seamlessly procrastinate client-based ROI without long-term high-impact.</p>
                                <a href="#" class="btn btn-primary btn-green btn-lg">Browse Courses</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="light-wrapper">
        <div class="section-inner">
            <div class="container">
                <div class="row mb90">
                    <div class="feature-box alt-style col-sm-4 wow fadeIn smoothie">
                        <div class="icon-box-1 match-height">
                            <i class="fa-4x pe-7s-camera smoothie"><a href="#">Details</a></i>
                            <div class="content-area">
                                <h3 class="title">Box Title</h3>
                                <div class="content">Collaboratively engage just in time human capital for intermandated niche markets.</div>
                            </div>
                        </div>
                    </div>
                    <div class="feature-box alt-style col-sm-4 wow fadeIn smoothie">
                        <div class="icon-box-1 match-height">
                            <i class="fa-4x pe-7s-star smoothie"><a href="#">Details</a></i>
                            <div class="content-area">
                                <h3 class="title">Box Title</h3>
                                <div class="content">Competently exploit efficient manufactured products for vertical channels. </div>
                            </div>
                        </div>
                    </div>
                    <div class="feature-box alt-style col-sm-4 wow fadeIn smoothie">
                        <div class="icon-box-1 match-height">
                            <i class="fa-4x pe-7s-light smoothie"><a href="#">Details</a></i>
                            <div class="content-area">
                                <h3 class="title">Box Title</h3>
                                <div class="content">ollaboratively engage just in time human capital for intermandated niche markets.</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-8">
                        <p class="lead mb40">Interactively visualize web-enabled markets vis-a-vis future-proof total linkage. Competently parallel task economically sound networks before effective systems. Assertively formulate premier methodologies rather than client-focused products. Energistically customize equity invested imperatives via ethical outsourcing. Competently parallel task compelling technologies through next-generation initiatives.</p>
                        <p>Energistically evisculate 2.0 strategic theme areas via extensible sources. Conveniently procrastinate client-focused models through out-of-the-box systems. Holisticly parallel task cross functional synergy rather than low-risk high-yield experiences. Conveniently myocardinate premier networks after seamless materials. Competently build technically sound applications after exceptional applications.</p>
                        <p>Competently impact multifunctional resources through cross-platform bandwidth. Globally reinvent B2C manufactured products and cross functional portals. Quickly seize cross-unit partnerships rather than premier data. Professionally evisculate value-added communities whereas reliable deliverables. Monotonectally parallel task e-business interfaces with distinctive resources.</p>
                    </div>

                    <div class="col-sm-4">
                        <div class="panel-group styled-accordion" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading panel-open smoothie" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Summer Special</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <p>Phosfluorescently e-enable multidisciplinary architectures without team driven services. Credibly innovate high-payoff alignments before resource sucking metrics. Uniquely repurpose granular synergy after tactical internal or "organic" sources. Conveniently fabricate go forward niches vis-a-vis top-line partnerships. Progressively brand 24/7 deliverables whereas diverse total linkage.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading smoothie" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Outdoor Persuits</a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading smoothie" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Autumn Event</a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <p>Enthusiastically disseminate bleeding-edge ROI rather than accurate results. Collaboratively scale interoperable experiences for dynamic services. Appropriately embrace web-enabled potentialities rather than proactive human capital. Distinctively mesh resource-leveling e-commerce without B2B applications. Rapidiously iterate resource sucking intellectual capital vis-a-vis alternative experiences. Dramatically exploit unique communities rather than customized customer service. Energistically pursue high standards in manufactured products after top-line expertise.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dark-wrapper">
        <div class="section-inner">
            <div class="container">
                <div class="row mb60 text-center">
                    <div class="col-sm-12">
                        <h3 class="section-title">Our Partners</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-sm-offset-1">
                        <div class="row">
                            <div class="col-md-3"><img class="img-responsive" src="{{asset('assets/images/clients/logo1.png')}}" alt=""></div>
                            <div class="col-md-3"><img class="img-responsive" src="{{asset('assets/images/clients/logo2.png')}}" alt=""></div>
                            <div class="col-md-3"><img class="img-responsive" src="{{asset('assets/images/clients/logo3.png')}}" alt=""></div>
                            <div class="col-md-3"><img class="img-responsive" src="{{asset('assets/images/clients/logo4.png')}}" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="our-team" class="white-wrapper">
        <div class="section-inner">
            <div class="container">
                <div class="row mb60 text-center">
                    <div class="col-sm-12">
                        <h3 class="section-title">Meet The Team</h3>
                        <p class="section-sub-title">The very best in their fields.</p>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row nopaddingleftright">
                    <div class="team-item col-md-6 match-height nopaddingleftright mb40">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="hover-effect smoothie match-height">
                                    <a href="#" class="smoothie">
                                        <img src="{{asset('assets/images/team-1.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <h3 class="nomargintop">Richard Smith</h3>
                                <p class="mb20">Credibly re-engineer best-of-breed imperatives for viral innovation. Appropriately extend sustainable channels before sustainable niches. Intrinsicly seize functionalized niches for user-centric paradigms.</p>
                                <div class="team-social text-left">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                    <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                                    <a class="blog" href="#"><i class="fa fa-rss"></i></a>
                                    <a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="team-item col-md-6 match-height nopaddingleftright mb40">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="hover-effect smoothie match-height">
                                    <a href="#" class="smoothie">
                                        <img src="{{asset('assets/images/team-2.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <h3 class="nomargintop">Richard Smith</h3>
                                <p class="mb20">Credibly re-engineer best-of-breed imperatives for viral innovation. Appropriately extend sustainable channels before sustainable niches. Intrinsicly seize functionalized niches for user-centric paradigms.</p>
                                <div class="team-social text-left">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                    <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                                    <a class="blog" href="#"><i class="fa fa-rss"></i></a>
                                    <a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="team-item col-md-6 match-height nopaddingleftright">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="hover-effect smoothie match-height">
                                    <a href="#" class="smoothie">
                                        <img src="{{asset('assets/images/team-3.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <h3 class="nomargintop">Richard Smith</h3>
                                <p class="mb20">Credibly re-engineer best-of-breed imperatives for viral innovation. Appropriately extend sustainable channels before sustainable niches. Intrinsicly seize functionalized niches for user-centric paradigms.</p>
                                <div class="team-social text-left">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                    <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                                    <a class="blog" href="#"><i class="fa fa-rss"></i></a>
                                    <a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="team-item col-md-6 match-height nopaddingleftright">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="hover-effect smoothie match-height">
                                    <a href="#" class="smoothie">
                                        <img src="{{asset('assets/images/team-4.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <h3 class="nomargintop">Richard Smith</h3>
                                <p class="mb20">Credibly re-engineer best-of-breed imperatives for viral innovation. Appropriately extend sustainable channels before sustainable niches. Intrinsicly seize functionalized niches for user-centric paradigms.</p>
                                <div class="team-social text-left">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                    <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                                    <a class="blog" href="#"><i class="fa fa-rss"></i></a>
                                    <a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="our-courses" class="white-wrapper">
        <div class="section-inner">
            <div class="container">
                <div class="row mb60 text-center">
                    <div class="col-sm-12">
                        <h3 class="section-title">The Works</h3>
                        <p class="section-sub-title">The care you need, quickly.</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="masonry-portfolio row">
                                    <ul class="masonry-portfolio-filter mb40 list-inline wow fadeIn text-center" data-wow-delay="0.2s">
                                        <li><a class="btn btn-primary btn-transparent active" href="#" data-filter="*">All</a></li>
                                        <li><a class="btn btn-primary btn-transparent" href="#" data-filter=".apps">Apps</a></li>
                                        <li><a class="btn btn-primary btn-transparent" href="#" data-filter=".design">Design</a></li>
                                        <li><a class="btn btn-primary btn-transparent" href="#" data-filter=".photography">Photography</a></li>
                                        <li><a class="btn btn-primary btn-transparent" href="#" data-filter=".video">Video</a></li>
                                    </ul>

                                    <div class="col-sm-12">
                                        <div class="masonry-portfolio-items">
                                            <div class="row">
                                                <div class="col-sm-4 masonry-portfolio-item apps nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset('assets/images/portfolio/folio-1.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry-portfolio-item design nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset('assets/images/portfolio/folio-2.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry-portfolio-item photography nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset('assets/images/portfolio/folio-3.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry-portfolio-item apps nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset('assets/images/portfolio/folio-4.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry-portfolio-item design nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset('assets/images/portfolio/folio-5.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry-portfolio-item design nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset('assets/images/portfolio/folio-6.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry-portfolio-item design nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset('assets/images/portfolio/folio-7.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="dark-wrapper green-wrapper">
        <div class="section-inner-60">
            <div class="container">
                <div class="row cta">
                    <div class="col-sm-8">
                        <h4>Want to pay us a visit?</h4>
                    </div>
                    <div class="col-sm-4 text-right">
                        <a href="#" id="open-map" class="btn btn-white btn-lg">View Map</a>
                        <a href="#" id="open-contact" class="btn btn-primary btn-lg">Email Us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="map-holder">
        <div id="mapwrapper"></div>
    </div>

    <div id="contact-holder" class="silver-wrapper">
        <div class="section-inner">
            <div class="vertical-center-js">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <p>Competently develop client-focused customer service whereas team driven leadership skills. Monotonectally synergize out-of-the-box platforms after next-generation web services. Phosfluorescently reintermediate state of the art methods of empowerment whereas excellent architectures. Professionally expedite distributed best practices after orthogonal human capital. Competently predominate resource-leveling materials via end-to-end ideas.</p>
                        </div>
                        <div class="col-sm-8 col-sm-offset-1">
                            <div class="row">
                                <div id="message" class="col-sm-12"></div>
                                <div class="col-sm-12">
                                    <form method="post" action="sendemail.php" id="contactform" class="main-contact-form">
                                        <div class="form-group">
                                            <input type="text" class="form-control col-md-4 mb20" name="name" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name." />
                                            <input type="text" class="form-control col-md-4 mb20" name="email" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address." />
                                            <input type="text" class="form-control col-md-4 mb20" name="website" placeholder="Your URL *" id="website" required data-validation-required-message="Please enter your web address." />
                                        </div>
                                        <textarea name="comments" class="form-control mb20" id="comments" placeholder="Your Message *" required data-validation-required-message="Please enter a message."></textarea>
                                        <input class="btn btn-primary mt30 pull-right" type="submit" name="submit" value="Submit" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
