@extends('app')
@section('pages')

@endsection
<header id="home" class="backstretched fullheight">
    <div class="dark-overlay fullheight">
        <div class="container fullheight">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-sm-11 col-xs-12">
                        <h1><small>Welcome To</small><br>
                            Benedict Co.</h1>
                        <p>
                            <a class="btn btn-white btn-lg page-scroll" href="#featured-news" role="button">Latest</a>
                            <a class="btn btn-lg btn-primary page-scrol" href="#projects" role="button">Projects</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<section id="featured-news" class="white-wrapper">
    <div class="section-inner nopaddingbottom">
        <div class="container">
            <div class="row mb60 text-center">
                <div class="col-sm-12">
                    <h3 class="section-title">Featured News</h3>
                    <p class="section-sub-title">Hot from the news desk.</p>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row post-tile">
                        <div class="col-lg-6 col-md-2 col-sm-4 col-xs-3 nopaddingleftright match-height">
                            <div class="hover-effect smoothie">
                                <a href="#" class="smoothie">
                                    <img src="{{route('assets/images/blog-square.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                                <div class="hover-caption dark-overlay smoothie text-center">
                                    <div class="vertical-center-js">
                                        <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8 col-sm-8 col-xs-9 blog-caption match-height pure-white-wrapper">
                            <div class="vertical-center-js">
                                <h4>Student Reaches Goal</h4>
                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies. Credibly monetize empowered portals before distinctive methodologies.</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="row post-tile-alt">
                        <div class="col-lg-6 col-md-8 col-sm-8 col-xs-9 blog-caption match-height pure-white-wrapper">
                            <div class="vertical-center-js text-right">
                                <h4>Student Reaches Goal</h4>
                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies. Credibly monetize empowered portals before distinctive methodologies.</small></p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-2 col-sm-4 col-xs-3 nopaddingleftright match-height">
                            <div class="hover-effect smoothie">
                                <a href="#" class="smoothie">
                                    <img src="{{route('assets/images/blog-square-2.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                                <div class="hover-caption dark-overlay smoothie text-center">
                                    <div class="vertical-center-js">
                                        <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row post-tile">
                        <div class="col-lg-6 col-md-2 col-sm-4 col-xs-3 nopaddingleftright match-height">
                            <div class="hover-effect smoothie">
                                <a href="#" class="smoothie">
                                    <img src="{{route('assets/images/blog-square-3.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                                <div class="hover-caption dark-overlay smoothie text-center">
                                    <div class="vertical-center-js">
                                        <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8 col-sm-8 col-xs-9 blog-caption match-height pure-white-wrapper">
                            <div class="vertical-center-js">
                                <h4>Student Reaches Goal</h4>
                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies. Credibly monetize empowered portals before distinctive methodologies.</small></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row post-tile">
                        <div class="col-lg-6 col-md-2 col-sm-4 col-xs-3 nopaddingleftright match-height">
                            <div class="hover-effect smoothie">
                                <a href="#" class="smoothie">
                                    <img src="{{route('assets/images/blog-square-4.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                                <div class="hover-caption dark-overlay smoothie text-center">
                                    <div class="vertical-center-js">
                                        <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8 col-sm-8 col-xs-9 blog-caption match-height pure-white-wrapper">
                            <div class="vertical-center-js">
                                <h4>Student Reaches Goal</h4>
                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies. Credibly monetize empowered portals before distinctive methodologies.</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="row post-tile-alt">
                        <div class="col-lg-6 col-md-8 col-sm-8 col-xs-9 blog-caption match-height pure-white-wrapper">
                            <div class="vertical-center-js text-right">
                                <h4>Student Reaches Goal</h4>
                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies. Credibly monetize empowered portals before distinctive methodologies.</small></p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-2 col-sm-4 col-xs-3 nopaddingleftright match-height">
                            <div class="hover-effect smoothie">
                                <a href="#" class="smoothie">
                                    <img src="{{route('assets/images/blog-square-5.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                                <div class="hover-caption dark-overlay smoothie text-center">
                                    <div class="vertical-center-js">
                                        <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row post-tile">
                        <div class="col-lg-6 col-md-2 col-sm-4 col-xs-3 nopaddingleftright match-height">
                            <div class="hover-effect smoothie">
                                <a href="#" class="smoothie">
                                    <img src="{{route('assets/images/blog-square-6.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                                <div class="hover-caption dark-overlay smoothie text-center">
                                    <div class="vertical-center-js">
                                        <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8 col-sm-8 col-xs-9 blog-caption match-height pure-white-wrapper">
                            <div class="vertical-center-js">
                                <h4>Student Reaches Goal</h4>
                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies. Credibly monetize empowered portals before distinctive methodologies.</small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="our-courses" class="pure-white-wrapper">
    <div class="section-inner nopaddingbottom">
        <div class="container">
            <div class="row mb60 text-center">
                <div class="col-sm-12">
                    <h3 class="section-title">Staff Picks</h3>
                    <p class="section-sub-title">Hot from the news desk.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="hover-shift smoothie match-height parallax" data-parallax="scroll" data-image-src="{{asset('assets/images/section-bg-1.jpg')}}" data-speed="0.8" data-mh="page-splitter">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-xs-10 dark-overlay col-sm-offset-7 fill-right smoothie">
                <div class="section-inner">
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10">
                            <h3>Philip Davies</h3>
                            <h5 class="mb40">Drums.</h5>
                            <p>Holisticly orchestrate superior total linkage rather than end-to-end internal or "organic" sources. Seamlessly procrastinate client-based ROI without long-term high-impact.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-1 member-social smoothie">
                <div class="section-inner">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-soundcloud"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="hover-shift smoothie match-height parallax" data-parallax="scroll" data-image-src="{{asset('assets/images/section-bg-2.jpg')}}" data-speed="0.8" data-mh="page-splitter">
    <div class="container">
        <div class="row">
            <div class="col-xs-1 member-social smoothie">
                <div class="section-inner">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-soundcloud"></i></a>
                </div>
            </div>
            <div class="col-md-4 dark-overlay fill-left smoothie match-height" data-mh="page-splitter">
                <div class="section-inner">
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10">
                            <h3>Arnold Clarke</h3>
                            <h5 class="mb40">Bass, Backing Vocals.</h5>
                            <p>Distinctively revolutionize value-added channels for clicks-and-mortar niche markets. Rapidiously exploit out-of-the-box intellectual capital without orthogonal e-services.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="projects" class="pure-white-wrapper">
    <div class="section-inner nopaddingbottom">
        <div class="container">
            <div class="row mb60 text-center">
                <div class="col-sm-12">
                    <h3 class="section-title">Recent Works</h3>
                    <p class="section-sub-title">We are aweomse, this is why.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <header class="owl-carousel remove-item-paddings" data-items="5">
                <div class="match-height nopaddingleftright">
                    <div class="hover-effect smoothie">
                        <a href="#" class="smoothie">
                            <img src="{{asset('assets/images/blog-tall-1.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                        <div class="hover-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <h4>Student Reaches Goal</h4>
                            </div>
                        </div>
                        <div class="hover-caption dark-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="match-height nopaddingleftright">
                    <div class="hover-effect smoothie">
                        <a href="#" class="smoothie">
                            <img src="{{asset('assets/images/blog-tall-2.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                        <div class="hover-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <h4>Student Reaches Goal</h4>
                            </div>
                        </div>
                        <div class="hover-caption dark-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="match-height nopaddingleftright">
                    <div class="hover-effect smoothie">
                        <a href="#" class="smoothie">
                            <img src="{{asset('assets/images/blog-tall-3.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                        <div class="hover-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <h4>Student Reaches Goal</h4>
                            </div>
                        </div>
                        <div class="hover-caption dark-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="match-height nopaddingleftright">
                    <div class="hover-effect smoothie">
                        <a href="#" class="smoothie">
                            <img src="{{asset('assets/images/blog-tall-4.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                        <div class="hover-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <h4>Student Reaches Goal</h4>
                            </div>
                        </div>
                        <div class="hover-caption dark-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="match-height nopaddingleftright">
                    <div class="hover-effect smoothie">
                        <a href="#" class="smoothie">
                            <img src="{{asset('assets/images/blog-tall-5.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                        <div class="hover-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <h4>Student Reaches Goal</h4>
                            </div>
                        </div>
                        <div class="hover-caption dark-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="match-height nopaddingleftright">
                    <div class="hover-effect smoothie">
                        <a href="#" class="smoothie">
                            <img src="{{asset('assets/images/blog-tall-6.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                        <div class="hover-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <h4>Student Reaches Goal</h4>
                            </div>
                        </div>
                        <div class="hover-caption dark-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
    </div>
</section>

<section class="dark-wrapper">
    <div class="section-inner">
        <div class="container">
            <div class="row mb60 text-center">
                <div class="col-sm-12">
                    <h3 class="section-title">Our Partners</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-sm-offset-1">
                    <div class="row">
                        <div class="col-md-3"><img class="img-responsive" src="{{asset('assets/images/clients/logo1.png')}}" alt=""></div>
                        <div class="col-md-3"><img class="img-responsive" src="{{asset('assets/images/clients/logo2.png')}}" alt=""></div>
                        <div class="col-md-3"><img class="img-responsive" src="{{asset('assets/images/clients/logo3.png')}}" alt=""></div>
                        <div class="col-md-3"><img class="img-responsive" src="{{asset('assets/images/clients/logo4.png')}}" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="our-team" class="white-wrapper">
    <div class="section-inner">
        <div class="container">
            <div class="row mb60 text-center">
                <div class="col-sm-12">
                    <h3 class="section-title">Meet The Team</h3>
                    <p class="section-sub-title">The very best in their fields.</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row nopaddingleftright">
                <div class="team-item col-md-6 match-height nopaddingleftright mb40">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="hover-effect smoothie match-height">
                                <a href="#" class="smoothie">
                                    <img src="{{asset('assets/images/team-1.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <h3 class="nomargintop">Richard Smith</h3>
                            <p class="mb20">Credibly re-engineer best-of-breed imperatives for viral innovation. Appropriately extend sustainable channels before sustainable niches. Intrinsicly seize functionalized niches for user-centric paradigms.</p>
                            <div class="team-social text-left">
                                <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                                <a class="blog" href="#"><i class="fa fa-rss"></i></a>
                                <a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="team-item col-md-6 match-height nopaddingleftright mb40">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="hover-effect smoothie match-height">
                                <a href="#" class="smoothie">
                                    <img src="{{asset('assets/images/team-2.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <h3 class="nomargintop">Richard Smith</h3>
                            <p class="mb20">Credibly re-engineer best-of-breed imperatives for viral innovation. Appropriately extend sustainable channels before sustainable niches. Intrinsicly seize functionalized niches for user-centric paradigms.</p>
                            <div class="team-social text-left">
                                <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                                <a class="blog" href="#"><i class="fa fa-rss"></i></a>
                                <a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="team-item col-md-6 match-height nopaddingleftright">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="hover-effect smoothie match-height">
                                <a href="#" class="smoothie">
                                    <img src="{{asset('assets/images/team-3.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <h3 class="nomargintop">Richard Smith</h3>
                            <p class="mb20">Credibly re-engineer best-of-breed imperatives for viral innovation. Appropriately extend sustainable channels before sustainable niches. Intrinsicly seize functionalized niches for user-centric paradigms.</p>
                            <div class="team-social text-left">
                                <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                                <a class="blog" href="#"><i class="fa fa-rss"></i></a>
                                <a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="team-item col-md-6 match-height nopaddingleftright">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="hover-effect smoothie match-height">
                                <a href="#" class="smoothie">
                                    <img src="{{asset('assets/images/team-4.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <h3 class="nomargintop">Richard Smith</h3>
                            <p class="mb20">Credibly re-engineer best-of-breed imperatives for viral innovation. Appropriately extend sustainable channels before sustainable niches. Intrinsicly seize functionalized niches for user-centric paradigms.</p>
                            <div class="team-social text-left">
                                <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                                <a class="blog" href="#"><i class="fa fa-rss"></i></a>
                                <a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dark-wrapper green-wrapper">
    <div class="section-inner-60">
        <div class="container">
            <div class="row cta">
                <div class="col-sm-8">
                    <h4>Want to pay us a visit?</h4>
                </div>
                <div class="col-sm-4 text-right">
                    <a href="#" id="open-map" class="btn btn-white btn-lg">View Map</a>
                    <a href="#" id="open-contact" class="btn btn-primary btn-lg">Email Us</a>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="map-holder">
    <div id="mapwrapper"></div>
</div>

<div id="contact-holder" class="silver-wrapper">
    <div class="section-inner">
        <div class="vertical-center-js">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <p>Competently develop client-focused customer service whereas team driven leadership skills. Monotonectally synergize out-of-the-box platforms after next-generation web services. Phosfluorescently reintermediate state of the art methods of empowerment whereas excellent architectures. Professionally expedite distributed best practices after orthogonal human capital. Competently predominate resource-leveling materials via end-to-end ideas.</p>
                    </div>
                    <div class="col-sm-8 col-sm-offset-1">
                        <div class="row">
                            <div id="message" class="col-sm-12"></div>
                            <div class="col-sm-12">
                                <form method="post" action="sendemail.php" id="contactform" class="main-contact-form">
                                    <div class="form-group">
                                        <input type="text" class="form-control col-md-4 mb20" name="name" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name." />
                                        <input type="text" class="form-control col-md-4 mb20" name="email" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address." />
                                        <input type="text" class="form-control col-md-4 mb20" name="website" placeholder="Your URL *" id="website" required data-validation-required-message="Please enter your web address." />
                                    </div>
                                    <textarea name="comments" class="form-control mb20" id="comments" placeholder="Your Message *" required data-validation-required-message="Please enter a message."></textarea>
                                    <input class="btn btn-primary mt30 pull-right" type="submit" name="submit" value="Submit" />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
