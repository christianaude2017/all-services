@extends('app')

@section('pages')

    <header id="home" class="fashion-slider fullheight">
        <div class="fullheight">
            <div class="container fullheight">
                <div class="jumbotron">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-6 col-xs-12">
                            <h1><small>Home Of The</small><br>
                                Fashionista</h1>
                            <p>
                                <a class="btn btn-white btn-lg page-scroll" href="#about" role="button">About Us</a>
                                <a class="btn btn-lg btn-primary" href="#" role="button">Latest Projects</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section id="our-courses" class="white-wrapper">
        <div class="section-inner nopaddingbottom">
            <div class="container">
                <div class="row mb60 text-center">
                    <div class="col-sm-12">
                        <h3 class="section-title">The Works</h3>
                        <p class="section-sub-title">The care you need, quickly.</p>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="masonry-portfolio row">
                                    <ul class="col-sm-2 masonry-portfolio-filter mb40 list-inline vertical wow fadeIn text-right" data-wow-delay="0.2s">
                                        <li><a class="btn btn-primary btn-transparent active" href="#" data-filter="*">All</a></li>
                                        <li><a class="btn btn-primary btn-transparent" href="#" data-filter=".apps">Apps</a></li>
                                        <li><a class="btn btn-primary btn-transparent" href="#" data-filter=".design">Design</a></li>
                                        <li><a class="btn btn-primary btn-transparent" href="#" data-filter=".photography">Photography</a></li>
                                        <li><a class="btn btn-primary btn-transparent" href="#" data-filter=".video">Video</a></li>
                                    </ul>

                                    <div class="col-sm-9">
                                        <div class="masonry-portfolio-items">
                                            <div class="row">
                                                <div class="col-sm-4 masonry-portfolio-item apps nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset('assets/images/portfolio/folio-1.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry-portfolio-item design nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset("assets/images/portfolio/folio-2.jpg")}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry-portfolio-item photography nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset("assets/images/portfolio/folio-3.jpg")}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry-portfolio-item apps nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset("assets/images/portfolio/folio-4.jpg")}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry-portfolio-item design nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset("assets/images/portfolio/folio-5.jpg")}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry-portfolio-item design nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset("assets/images/portfolio/folio-6.jpg")}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry-portfolio-item design nopaddingleftright">
                                                    <div class="hover-effect smoothie">
                                                        <a href="#" class="smoothie">
                                                            <img src="{{asset('assets/images/portfolio/folio-7.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                                        <div class="hover-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <h4>Project Title</h4>
                                                            </div>
                                                        </div>
                                                        <div class="hover-caption dark-overlay smoothie text-center">
                                                            <div class="vertical-center-js">
                                                                <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                                <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id="our-team" class="white-wrapper">
        <div class="section-inner">
            <div class="container">
                <div class="row mb60 text-center">
                    <div class="col-sm-12">
                        <h3 class="section-title">Meet The Team</h3>
                        <p class="section-sub-title">The very best in their fields.</p>
                    </div>
                </div>
            </div>

            <div class="row nopaddingleftright">
                <div class="team-item col-md-3 match-height nopaddingleftright">
                    <div class="hover-effect smoothie match-height">
                        <a href="#" class="smoothie">
                            <img src="{{'assets/images/team-1.jpg'}}" alt="Image" class="img-responsive smoothie"></a>
                        <div class="hover-caption dark-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <h3>Richard Smith</h3>
                                <p class="lead mb20">Credibly re-engineer best-of-breed imperatives for viral innovation. Appropriately extend sustainable channels before sustainable niches. Intrinsicly seize functionalized niches for user-centric paradigms.</p>
                                <div class="team-social">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                    <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                                    <a class="blog" href="#"><i class="fa fa-rss"></i></a>
                                    <a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="team-item col-md-3 match-height nopaddingleftright">
                    <div class="hover-effect smoothie match-height">
                        <a href="#" class="smoothie">
                            <img src="{{'assets/images/team-2.jpg'}}" alt="Image" class="img-responsive smoothie"></a>
                        <div class="hover-caption dark-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <h4>Dave Edwards</h4>
                                <p class="mb20">Credibly re-engineer best-of-breed imperatives for viral innovation. Appropriately extend sustainable channels before sustainable niches. Intrinsicly seize functionalized niches for user-centric paradigms.</p>
                                <a href="single-team.html" class="btn btn-primary btn-green">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="team-item col-md-3 match-height nopaddingleftright">
                    <div class="hover-effect smoothie match-height">
                        <a href="#" class="smoothie">
                            <img src="{{'assets/images/team-3.jpg'}}" alt="Image" class="img-responsive smoothie"></a>
                        <div class="hover-caption dark-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <h4>Lisa Rhymes</h4>
                                <p class="mb20">Credibly re-engineer best-of-breed imperatives for viral innovation. Appropriately extend sustainable channels before sustainable niches. Intrinsicly seize functionalized niches for user-centric paradigms.</p>
                                <a href="single-team.html" class="btn btn-primary btn-green">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="team-item col-md-3 match-height nopaddingleftright">
                    <div class="hover-effect smoothie match-height">
                        <a href="#" class="smoothie">
                            <img src="{{'assets/images/team-4.jpg'}}" alt="Image" class="img-responsive smoothie"></a>
                        <div class="hover-caption dark-overlay smoothie text-center">
                            <div class="vertical-center-js">
                                <h4>Steve Kane</h4>
                                <p class="mb20">Credibly re-engineer best-of-breed imperatives for viral innovation. Appropriately extend sustainable channels before sustainable niches. Intrinsicly seize functionalized niches for user-centric paradigms.</p>
                                <a href="single-team.html" class="btn btn-primary btn-green">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="our-courses" class="white-wrapper">
        <div class="section-inner nopaddingbottom">
            <div class="container">
                <div class="row mb60 text-center">
                    <div class="col-sm-12">
                        <h3 class="section-title">The Works</h3>
                        <p class="section-sub-title">The care you need, quickly.</p>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="masonry-portfolio row">
                            <ul class="masonry-portfolio-filter mb40 list-inline wow fadeIn text-center" data-wow-delay="0.2s">
                                <li><a class="btn btn-primary btn-transparent active" href="#" data-filter="*">All</a></li>
                                <li><a class="btn btn-primary btn-transparent" href="#" data-filter=".apps">Apps</a></li>
                                <li><a class="btn btn-primary btn-transparent" href="#" data-filter=".design">Design</a></li>
                                <li><a class="btn btn-primary btn-transparent" href="#" data-filter=".photography">Photography</a></li>
                                <li><a class="btn btn-primary btn-transparent" href="#" data-filter=".video">Video</a></li>
                            </ul>

                            <div class="">
                                <div class="masonry-portfolio-items">
                                    <div class="row">
                                        <div class="col-sm-4 masonry-portfolio-item apps nopaddingleftright">
                                            <div class="hover-effect smoothie">
                                                <a href="#" class="smoothie">
                                                    <img src="{{asset('assets/images/portfolio/folio-1.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                                <div class="hover-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <h4>Project Title</h4>
                                                    </div>
                                                </div>
                                                <div class="hover-caption dark-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                        <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 masonry-portfolio-item design nopaddingleftright">
                                            <div class="hover-effect smoothie">
                                                <a href="#" class="smoothie">
                                                    <img src="{{asset("assets/images/portfolio/folio-2.jpg")}}" alt="Image" class="img-responsive smoothie"></a>
                                                <div class="hover-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <h4>Project Title</h4>
                                                    </div>
                                                </div>
                                                <div class="hover-caption dark-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                        <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 masonry-portfolio-item photography nopaddingleftright">
                                            <div class="hover-effect smoothie">
                                                <a href="#" class="smoothie">
                                                    <img src="{{asset("assets/images/portfolio/folio-3.jpg")}}" alt="Image" class="img-responsive smoothie"></a>
                                                <div class="hover-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <h4>Project Title</h4>
                                                    </div>
                                                </div>
                                                <div class="hover-caption dark-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                        <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 masonry-portfolio-item apps nopaddingleftright">
                                            <div class="hover-effect smoothie">
                                                <a href="#" class="smoothie">
                                                    <img src="{{asset("assets/images/portfolio/folio-4.jpg")}}" alt="Image" class="img-responsive smoothie"></a>
                                                <div class="hover-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <h4>Project Title</h4>
                                                    </div>
                                                </div>
                                                <div class="hover-caption dark-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                        <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 masonry-portfolio-item design nopaddingleftright">
                                            <div class="hover-effect smoothie">
                                                <a href="#" class="smoothie">
                                                    <img src="{{asset("assets/images/portfolio/folio-5.jpg")}}" alt="Image" class="img-responsive smoothie"></a>
                                                <div class="hover-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <h4>Project Title</h4>
                                                    </div>
                                                </div>
                                                <div class="hover-caption dark-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                        <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 masonry-portfolio-item design nopaddingleftright">
                                            <div class="hover-effect smoothie">
                                                <a href="#" class="smoothie">
                                                    <img src="{{asset("assets/images/portfolio/folio-6.jpg")}}" alt="Image" class="img-responsive smoothie"></a>
                                                <div class="hover-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <h4>Project Title</h4>
                                                    </div>
                                                </div>
                                                <div class="hover-caption dark-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                        <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 masonry-portfolio-item design nopaddingleftright">
                                            <div class="hover-effect smoothie">
                                                <a href="#" class="smoothie">
                                                    <img src="{{asset('assets/images/portfolio/folio-7.jpg')}}" alt="Image" class="img-responsive smoothie"></a>
                                                <div class="hover-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <h4>Project Title</h4>
                                                    </div>
                                                </div>
                                                <div class="hover-caption dark-overlay smoothie text-center">
                                                    <div class="vertical-center-js">
                                                        <p class="mb20"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                                        <a href="{{route('singlePortfolio')}}" class="btn btn-primary btn-green">View Project</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="dark-wrapper green-wrapper">
        <div class="section-inner-60">
            <div class="container">
                <div class="row cta">
                    <div class="col-sm-8">
                        <h4>Want to pay us a visit?</h4>
                    </div>
                    <div class="col-sm-4 text-right">
                        <a href="#" id="open-map" class="btn btn-white btn-lg">View Map</a>
                        <a href="#" id="open-contact" class="btn btn-primary btn-lg">Email Us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="map-holder">
        <div id="mapwrapper"></div>
    </div>

    <div id="contact-holder" class="silver-wrapper">
        <div class="section-inner">
            <div class="vertical-center-js">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <p>Competently develop client-focused customer service whereas team driven leadership skills. Monotonectally synergize out-of-the-box platforms after next-generation web services. Phosfluorescently reintermediate state of the art methods of empowerment whereas excellent architectures. Professionally expedite distributed best practices after orthogonal human capital. Competently predominate resource-leveling materials via end-to-end ideas.</p>
                        </div>
                        <div class="col-sm-8 col-sm-offset-1">
                            <div class="row">
                                <div id="message" class="col-sm-12"></div>
                                <div class="col-sm-12">
                                    <form method="post" action="sendemail.php" id="contactform" class="main-contact-form">
                                        <div class="form-group">
                                            <input type="text" class="form-control col-md-4 mb20" name="name" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name." />
                                            <input type="text" class="form-control col-md-4 mb20" name="email" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address." />
                                            <input type="text" class="form-control col-md-4 mb20" name="website" placeholder="Your URL *" id="website" required data-validation-required-message="Please enter your web address." />
                                        </div>
                                        <textarea name="comments" class="form-control mb20" id="comments" placeholder="Your Message *" required data-validation-required-message="Please enter a message."></textarea>
                                        <input class="btn btn-primary mt30 pull-right" type="submit" name="submit" value="Submit" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
