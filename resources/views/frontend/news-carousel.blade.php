@extends('app')

@section('pages')
    <header id="home" class="backstretched single-page-hero">
        <div class="dark-overlay single-page-hero">
            <div class="container single-page-hero">
                <div class="vertical-center-js">
                    <h1 class="section-title mb20">News Carousel</h1>
                </div>
            </div>
        </div>
    </header>

    <section id="projects" class="dark-wrapper">
        <div class="nopaddingbottom">
            <div class="row">
                <header class="owl-carousel remove-item-paddings" data-items="3">
                    <div class="match-height nopaddingleftright">
                        <div class="hover-effect smoothie">
                            <a href="#" class="smoothie">
                                <img src="{{asset('assets/images/blog-tall-1.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                            <div class="hover-overlay smoothie text-center">
                                <div class="vertical-center-js">
                                    <h4>How to Avoid Bad Wine</h4>
                                </div>
                            </div>
                            <div class="hover-caption dark-overlay smoothie text-center">
                                <div class="vertical-center-js">
                                    <p class="mb60"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                    <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="match-height nopaddingleftright">
                        <div class="hover-effect smoothie">
                            <a href="#" class="smoothie">
                                <img src="{{asset('assets/images/blog-tall-2.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                            <div class="hover-overlay smoothie text-center">
                                <div class="vertical-center-js">
                                    <h4>Why Are Forests Dying?</h4>
                                </div>
                            </div>
                            <div class="hover-caption dark-overlay smoothie text-center">
                                <div class="vertical-center-js">
                                    <p class="mb60"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                    <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="match-height nopaddingleftright">
                        <div class="hover-effect smoothie">
                            <a href="#" class="smoothie">
                                <img src="{{asset('assets/images/blog-tall-3.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                            <div class="hover-overlay smoothie text-center">
                                <div class="vertical-center-js">
                                    <h4>Is Coffee Killing You?</h4>
                                </div>
                            </div>
                            <div class="hover-caption dark-overlay smoothie text-center">
                                <div class="vertical-center-js">
                                    <p class="mb60"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                    <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="match-height nopaddingleftright">
                        <div class="hover-effect smoothie">
                            <a href="#" class="smoothie">
                                <img src="{{asset('assets/images/blog-tall-4.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                            <div class="hover-overlay smoothie text-center">
                                <div class="vertical-center-js">
                                    <h4>Student Reaches Goal</h4>
                                </div>
                            </div>
                            <div class="hover-caption dark-overlay smoothie text-center">
                                <div class="vertical-center-js">
                                    <p class="mb60"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                    <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="match-height nopaddingleftright">
                        <div class="hover-effect smoothie">
                            <a href="#" class="smoothie">
                                <img src="{{asset('assets/images/blog-tall-5.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                            <div class="hover-overlay smoothie text-center">
                                <div class="vertical-center-js">
                                    <h4>Traveling Naked at Night</h4>
                                </div>
                            </div>
                            <div class="hover-caption dark-overlay smoothie text-center">
                                <div class="vertical-center-js">
                                    <p class="mb60"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                    <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="match-height nopaddingleftright">
                        <div class="hover-effect smoothie">
                            <a href="#" class="smoothie">
                                <img src="{{asset('assets/images/blog-tall-6.jpeg')}}" alt="Image" class="img-responsive smoothie"></a>
                            <div class="hover-overlay smoothie text-center">
                                <div class="vertical-center-js">
                                    <h4>New Yorks at Night in the 80s</h4>
                                </div>
                            </div>
                            <div class="hover-caption dark-overlay smoothie text-center">
                                <div class="vertical-center-js">
                                    <p class="mb60"><small>Credibly monetize empowered portals before distinctive methodologies.</small></p>
                                    <a href="{{route('post')}}" class="btn btn-primary btn-green">View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        </div>
    </section>

    <section class="dark-wrapper green-wrapper">
        <div class="section-inner-60">
            <div class="container">
                <div class="row cta">
                    <div class="col-sm-8">
                        <h4>Want to pay us a visit?</h4>
                    </div>
                    <div class="col-sm-4 text-right">
                        <a href="#" id="open-map" class="btn btn-white btn-lg">View Map</a>
                        <a href="#" id="open-contact" class="btn btn-primary btn-lg">Email Us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="map-holder">
        <div id="mapwrapper"></div>
    </div>

    <div id="contact-holder" class="silver-wrapper">
        <div class="section-inner">
            <div class="vertical-center-js">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <p>Competently develop client-focused customer service whereas team driven leadership skills. Monotonectally synergize out-of-the-box platforms after next-generation web services. Phosfluorescently reintermediate state of the art methods of empowerment whereas excellent architectures. Professionally expedite distributed best practices after orthogonal human capital. Competently predominate resource-leveling materials via end-to-end ideas.</p>
                        </div>
                        <div class="col-sm-8 col-sm-offset-1">
                            <div class="row">
                                <div id="message" class="col-sm-12"></div>
                                <div class="col-sm-12">
                                    <form method="post" action="sendemail.php" id="contactform" class="main-contact-form">
                                        <div class="form-group">
                                            <input type="text" class="form-control col-md-4 mb20" name="name" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name." />
                                            <input type="text" class="form-control col-md-4 mb20" name="email" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address." />
                                            <input type="text" class="form-control col-md-4 mb20" name="website" placeholder="Your URL *" id="website" required data-validation-required-message="Please enter your web address." />
                                        </div>
                                        <textarea name="comments" class="form-control mb20" id="comments" placeholder="Your Message *" required data-validation-required-message="Please enter a message."></textarea>
                                        <input class="btn btn-primary mt30 pull-right" type="submit" name="submit" value="Submit" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
