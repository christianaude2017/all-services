@extends('app')

@section('pages')
    <header id="home" class="error404-bg fullheight">
        <div class="dark-overlay fullheight">
            <div class="container fullheight">
                <div class="jumbotron">
                    <div class="row">
                        <div class="col-sm-11 col-xs-12">
                            <h1><small>Oh bother</small><br>
                                We got a 404 here.</h1>
                            <p>
                                <a class="btn btn-white btn-lg" href="{{route('home')}}" role="button">Home</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="dark-wrapper green-wrapper">
        <div class="section-inner-60">
            <div class="container">
                <div class="row cta">
                    <div class="col-sm-8">
                        <h4>Want to pay us a visit?</h4>
                    </div>
                    <div class="col-sm-4 text-right">
                        <a href="#" id="open-map" class="btn btn-white btn-lg">View Map</a>
                        <a href="#" id="open-contact" class="btn btn-primary btn-lg">Email Us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="map-holder">
        <div id="mapwrapper"></div>
    </div>

    <div id="contact-holder" class="silver-wrapper">
        <div class="section-inner">
            <div class="vertical-center-js">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <p>Competently develop client-focused customer service whereas team driven leadership skills. Monotonectally synergize out-of-the-box platforms after next-generation web services. Phosfluorescently reintermediate state of the art methods of empowerment whereas excellent architectures. Professionally expedite distributed best practices after orthogonal human capital. Competently predominate resource-leveling materials via end-to-end ideas.</p>
                        </div>
                        <div class="col-sm-8 col-sm-offset-1">
                            <div class="row">
                                <div id="message" class="col-sm-12"></div>
                                <div class="col-sm-12">
                                    <form method="post" action="sendemail.php" id="contactform" class="main-contact-form">
                                        <div class="form-group">
                                            <input type="text" class="form-control col-md-4 mb20" name="name" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name." />
                                            <input type="text" class="form-control col-md-4 mb20" name="email" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address." />
                                            <input type="text" class="form-control col-md-4 mb20" name="website" placeholder="Your URL *" id="website" required data-validation-required-message="Please enter your web address." />
                                        </div>
                                        <textarea name="comments" class="form-control mb20" id="comments" placeholder="Your Message *" required data-validation-required-message="Please enter a message."></textarea>
                                        <input class="btn btn-primary mt30 pull-right" type="submit" name="submit" value="Submit" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


