@extends('app')

@section('pages')
    <header id="home" class="backstretched single-page-hero">
        <div class="dark-overlay single-page-hero">
            <div class="container single-page-hero">
                <div class="vertical-center-js">
                    <h1 class="section-title mb20">Contact Us</h1>
                </div>
            </div>
        </div>
    </header>

    <div class="white-wrapper">
        <div class="section-inner">
            <div class="vertical-center-js">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <p>Competently develop client-focused customer service whereas team driven leadership skills. Monotonectally synergize out-of-the-box platforms after next-generation web services. Phosfluorescently reintermediate state of the art methods of empowerment whereas excellent architectures. Professionally expedite distributed best practices after orthogonal human capital. Competently predominate resource-leveling materials via end-to-end ideas.</p>
                        </div>
                        <div class="col-sm-8 col-sm-offset-1">
                            <div class="row">
                                <div id="message" class="col-sm-12"></div>
                                <div class="col-sm-12">
                                    <form method="post" action="sendemail.php" id="contactform" class="main-contact-form">
                                        <div class="form-group">
                                            <input type="text" class="form-control col-md-4 mb20" name="name" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name." />
                                            <input type="text" class="form-control col-md-4 mb20" name="email" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address." />
                                            <input type="text" class="form-control col-md-4 mb20" name="website" placeholder="Your URL *" id="website" required data-validation-required-message="Please enter your web address." />
                                        </div>
                                        <textarea name="comments" class="form-control mb20" id="comments" placeholder="Your Message *" required data-validation-required-message="Please enter a message."></textarea>
                                        <input class="btn btn-primary mt30 pull-right" type="submit" name="submit" value="Submit" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="map-holder-open">
        <div id="mapwrapper-open"></div>
    </div>

@endsection
